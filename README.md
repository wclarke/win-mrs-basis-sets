# FSL-MRS Basis Sets for WIN MRS Sequences

Repository for the creation and storage of basis spectra for sequences used at WIN (The Wellcome Centre for Integrative Neuroimaging), University of Oxford.

Simulation are performed using FSL-MRS's `fsl_mrs_sim` function.

## Requirements
Install FSL-MRS and associated tools by following the instructions at [fsl-mrs.com](https://open.win.ox.ac.uk/pages/fsl/fsl_mrs/install.html#option-1-using-conda).

## Use
0. Activate your `FSL-MRS` environment and cd to the desired directory.
```
conda activate my_fsl_mrs_env
cd sequences/{sequence_name}
```
1. Generate the sequence description json file using the generate.py script in a sequence folder. Provide any sequence timing information and an output name.

```
python generate.py
```

2. Run fsl_mrs_sim on the generated json using the script generated in the output directory (or manually for some older sequences)

```
cd output_dir
./run_sim.sh
```

Some sequences also generate a test script running on a few metabolites, so that you can verify the simulation before investing lots of time in simulation.
```
cd output_dir
./run_test_sim.sh
basis_tools vis {simulation_output}
```

Note, you might have to give execute permissions on these shell scripts `chmod u+x run_sim.sh run_test_sim.sh`.

3. Verify the simulation by running the `basis_tools` visualisation tools.
```
basis_tools vis {simulation_output}
```
![Example output of basis_tools vis](win_basis_logo.png){height=400px}  
*Example output of `basis_tools vis` run on a short TE basis set.*

## Credit
If you use these basis sets, please link to this repository and cite:

```
Clarke WT, Stagg CJ, Jbabdi S. FSL-MRS: An end-to-end spectroscopy analysis package. Magnetic Resonance in Medicine 2021;85:2950–2964 doi: 10.1002/mrm.28630.
```

## Sequences

### Siemens
1. sead_uzay_gui  
2. uzay_steam_gui  
3. uzay_svs_mpress  

*From the CMRR Spectro Package*  

4. eja_svs_slaser
5. eja_svs_mslaser

### Bruker

6. mt_slaser

