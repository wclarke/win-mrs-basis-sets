# eja_svs_mslaser

This is the the [CMRR Spectro Package](https://www.cmrr.umn.edu/spectro/) MEGA sLASER sequence.

## Timings calculated from repeated simulations.
See `timings/timing_calculations.xlsx`.

Timings are defined in five tau values (1-5) representing the sLASER pulse spacings (centroid to centroid). The spacing to the mega pulses is then further calculated.

```
raster_correction = 10 * ceil(0.1 * T_mega/2) - T_mega/2
padding = 3440 + 2 * raster_correction

tau1 = T_exc/2 + T_ref/2 + T_mega + padding
tau2 = T_ref + 3440
tau3 = tau2
tau4 = TE/2 - tau2
tau5 = tau4 - tau1

mega1_ref1 = T_ref/2 + T_mega/2 + padding/2
exc1_mega1 = tau1 - mega1_ref1

ref3_mega2 = mega1_ref1
mega2_ref4 = tau4 - ref3_mega2
```

#### Delays
```
delay1 = exc1_mega1 - (T_exc/2 + T_mega/2)  # exc to mega1
delay2 = mega1_ref1 - (T_ref/2 + T_mega/2)  # mega to ref1
delay3 = tau2 - T_ref                       # ref1 to ref2
delay4 = tau3 - T_ref                       # ref2 to ref3
delay5 = ref3_mega2 - (T_ref/2 + T_mega/2)  # ref3 to mega2
delay6 = mega2_ref4 - (T_ref/2 + T_mega/2)  # mega2 to ref4
delay7 = tau5 - T_ref/2                     # ref4 to ADC
```

### OLD - Timings for the 78 ms TE

```
Excite at 1210, for 2520, centre at  1210 + 2520/2 = 2470
MEGA1 at 5452, for 11776, centre at 5452 + 11776/2 = 11340
RF1 at 18950, for 8500, centre at 18950 + 4250 = 23200 
RF2 at 30890, for 8500, centre at 30890 + 4250 = 35140 
RF3 at 42830, for 8500, centre at 42830 + 4250 = 47080
MEGA2 at 53052, for 11776, centre at 53052 + 11776/2 = 58940
RF4 at 69890, for 8500, centre at 69890 + 4250 = 74140 
ADC at 80137
TE = 80137 - 2470  = 77667
```
Note that as with all `eja` sequences the ADC starts at almost the earliest available opportunity. As such the TE appears too short in the simulator, extra ADC points are discarded in post-processing.

### Calculate Delays
```
delay 1 = 5452 - (1210 + 2520) = 1722
delay 2 = 18950 - (5452 + 11776) = 1722
delay 3 = 30890 - (18950 + 8500) = 3440
delay 4 = 42830 - (30890 + 8500) = 3440
delay 5 = 53052 - (42830 + 8500) = 1722
delay 6 = 69890 - (53052 + 11776) = 5062
delay 7 = 80137 - (69890 + 8500) = 1747
```
Add 333 to final delay to get well phased echo and a TE=78

These delays are currently hardcoded in the generator.

## Pulses
As found in the binary:
 - Excitation is hsinc_400_8750.hsinc_400_8750 
 - Edit at 7T is SLR25600.INR5_OUTR1
 - Edit at 3T is GAUSS256.CUT01_FWHM60 (not yet implemented here)

VAPOR water suppression: 
135 Hz BW = 33900
Possibly P10.P10TR1_500
