# Timings for Lac editing setup

Baseline excitation = 3000 us  
Baseline refocus = 8500 us  
Baseline MEGA pulse width = 260 Hz  

Start from 68.0 ms TE

## 68 ms TE
****TIMING: SEMI-LASER*******************
90 pulse #1        = 2710
180 pulse #1       = 17800
180 pulse #2       = 29740
180 pulse #3       = 41680
MEGA pulse #1      = 8880
180 pulse #4       = 63740
MEGA pulse #2      = 50600
acq start          = 70710
actual tau1        = 15090
actual tau2        = 11940
actual tau3        = 11940
actual tau4        = 22060
expected tau5      = 6970
  actual tau5      = 6970
expected te        = 68000
  actual te        = 68000
MEGA pulse dur.    = 5888
****END TIMING***************************

## 75 ms TE
****TIMING: SEMI-LASER*******************
90 pulse #1        = 2710
180 pulse #1       = 17800
180 pulse #2       = 29740
180 pulse #3       = 41680
MEGA pulse #1      = 8880
180 pulse #4       = 67240
MEGA pulse #2      = 50600
acq start          = 77710
actual tau1        = 15090
actual tau2        = 11940
actual tau3        = 11940
actual tau4        = 25560
expected tau5      = 10470
  actual tau5      = 10470
expected te        = 75000
  actual te        = 75000
MEGA pulse dur.    = 5888
****END TIMING***************************

## 80 ms TE
****TIMING: SEMI-LASER*******************
90 pulse #1        = 2710
180 pulse #1       = 17800
180 pulse #2       = 29740
180 pulse #3       = 41680
MEGA pulse #1      = 8880
180 pulse #4       = 69740
MEGA pulse #2      = 50600
acq start          = 82710
actual tau1        = 15090
actual tau2        = 11940
actual tau3        = 11940
actual tau4        = 28060
expected tau5      = 12970
  actual tau5      = 12970
expected te        = 80000
  actual te        = 80000
MEGA pulse dur.    = 5888
****END TIMING***************************

## 80 ms 10 ms  ref
****TIMING: SEMI-LASER*******************
90 pulse #1        = 2710
180 pulse #1       = 18550
180 pulse #2       = 31990
180 pulse #3       = 45430
MEGA pulse #1      = 8880
180 pulse #4       = 71990
MEGA pulse #2      = 55100
acq start          = 82710
actual tau1        = 15840
actual tau2        = 13440
actual tau3        = 13440
actual tau4        = 26560
expected tau5      = 10720
  actual tau5      = 10720
expected te        = 80000
  actual te        = 80000
MEGA pulse dur.    = 5888

## 80 ms 11 ref
****TIMING: SEMI-LASER*******************
90 pulse #1        = 2710
180 pulse #1       = 19050
180 pulse #2       = 33490
180 pulse #3       = 47930
MEGA pulse #1      = 8880
180 pulse #4       = 73490
MEGA pulse #2      = 58100
acq start          = 82710
actual tau1        = 16340
actual tau2        = 14440
actual tau3        = 14440
actual tau4        = 25560
expected tau5      = 9220
  actual tau5      = 9220
expected te        = 80000
  actual te        = 80000
MEGA pulse dur.    = 5888
****END TIMING***************************


## 80 4 exc

****TIMING: SEMI-LASER*******************
90 pulse #1        = 3210
180 pulse #1       = 18800
180 pulse #2       = 30740
180 pulse #3       = 42680
MEGA pulse #1      = 9880
180 pulse #4       = 70740
MEGA pulse #2      = 51600
acq start          = 83210
actual tau1        = 15590
actual tau2        = 11940
actual tau3        = 11940
actual tau4        = 28060
expected tau5      = 12470
  actual tau5      = 12470
expected te        = 80000
  actual te        = 80000
MEGA pulse dur.    = 5888
****END TIMING***************************

## 80 ms 500 hz mega
****TIMING: SEMI-LASER*******************
90 pulse #1        = 2710
180 pulse #1       = 14980
180 pulse #2       = 26920
180 pulse #3       = 38860
MEGA pulse #1      = 7470
180 pulse #4       = 66920
MEGA pulse #2      = 46370
acq start          = 82710
actual tau1        = 12270
actual tau2        = 11940
actual tau3        = 11940
actual tau4        = 28060
expected tau5      = 15790
  actual tau5      = 15790
expected te        = 80000
  actual te        = 80000
MEGA pulse dur.    = 3072
