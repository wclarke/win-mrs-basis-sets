"""Generate the sequence description json for the eja_svs_mslaser sequence.
"""
import argparse
from pathlib import Path
import json
import datetime

import numpy as np
import pandas as pd

# Pulse file
base_path = Path(__file__).parent
pf_exc = base_path / 'pulses' / 'hsinc_400_8750.hsinc_400_8750.pta'
pf_edit = base_path / 'pulses' / 'SLR25600.INR5_OUTR1.pta'

EXC_AMP_INT = 45.4907  # Manually copied from file
EDIT_AMP_INT = 277.145997509  # Manually copied from file

parser = argparse.ArgumentParser(
    description='Create sequence description for eja_svs_mslaser (TE=78)')
parser.add_argument('--output', metavar='Dir', type=Path, default=Path('.'),
                    help='Output directory, defaults to cwd')
parser.add_argument('--water-suppression', action="store_true")
parser.add_argument('--te', type=float, default=78.0,
                    help='Echo times in ms, defaults to 78.0')
parser.add_argument('--pulse_dur_exc', type=float, default=2.520,
                    help='Excitation pulse duration in ms.'
                         ' Defaults to 2.520 ms')
parser.add_argument('--pulse_dur_ref', type=float, default=8.5,
                    help='Refocussing pulse duration in ms.'
                         ' Defaults to 8.5 ms')
parser.add_argument('--hsn', type=float, default=1.0,
                    help='Hyperbolic secant refocussing pulse N parameter.'
                         ' Defaults to 1.0.')
parser.add_argument('--hsr', type=float, default=20.0,
                    help='Hyperbolic secant refocussing pulse R (tbwp) parameter.'
                         ' Defaults to 20')
parser.add_argument('--edit_ppm', type=float, default=[7.0, 1.9], nargs=2,
                    help='Edit pulses ppm positions, defaults to (7.0, 1.9).')
parser.add_argument('--pulse_dur_edit', type=float, default=11.776,
                    help='Editing pulse duration in ms.'
                    ' Defaults to 130 Hz = (11.776 ms)')
parser.add_argument('--resolution', type=int, default=40,
                    help='Spatial simulation resolution, default=40')
args = parser.parse_args()

if args.water_suppression:
    pf_ws = 'pulses/P10.P10TR1_500pts.pta'
    WS_AMP_INT = 114.195816  # Manualy coppied from file
    # Assume 135 Hz bw = 33900 us duration
    pulse_dur_ws = 10

    pulse_df = pd.read_csv(
        pf_ws,
        sep=r"\s+",
        index_col=False,
        skip_blank_lines=True,
        names=['Amplitude', 'Phase'],
        skiprows=8)

    amp_int_rel = WS_AMP_INT / pulse_df['Amplitude'].size
    fa = 90
    pulse_length_ws = pulse_dur_ws / 1E3  # In s.
    pulse_amp_hz = 500 * (1E-3/pulse_length_ws) * (fa/180) * (1/amp_int_rel)
    ws_pulse_hz = pulse_df['Amplitude'].to_numpy()\
        * np.exp(1j * pulse_df['Phase'].to_numpy())
    ws_pulse_hz *= pulse_amp_hz

# Excitation pulse calculation
pulse_df = pd.read_csv(
    pf_exc,
    sep=r"\s+",
    index_col=False,
    skip_blank_lines=True,
    names=['Amplitude', 'Phase'],
    skiprows=8)

amp_int_rel = EXC_AMP_INT / pulse_df['Amplitude'].size
fa = 90
pulse_length_exc = args.pulse_dur_exc / 1E3  # In s.
pulse_amp_hz = 500 * (1E-3/pulse_length_exc) * (fa/180) * (1/amp_int_rel)
exc_pulse_hz = pulse_df['Amplitude'].to_numpy()\
    * np.exp(1j * pulse_df['Phase'].to_numpy())
exc_pulse_hz *= pulse_amp_hz

# Refocussing pulse calculation
pulse_length_ref = args.pulse_dur_ref / 1E3  # In s.
ref_points = 200
taxis = np.linspace(0, pulse_length_ref, ref_points)

N = args.hsn
r = args.hsr
tau = 1 - (2 * taxis / pulse_length_ref)
beta = 5.299
bw = r / pulse_length_ref
amp = 1 / np.cosh(beta * (tau ** N))
fm = bw * (1 - 2 * (np.cumsum(amp**2)/ np.sum(amp**2)))
pm = 2 * np.pi * np.cumsum(fm) * taxis[1]

ref_pulse_cmplx = amp * np.exp(1j*pm)

# Editing pulse calculation
pulse_df = pd.read_csv(
    pf_edit,
    sep=r"\s+",
    index_col=False,
    skip_blank_lines=True,
    names=['Amplitude', 'Phase'],
    skiprows=8)

amp_int_rel = EDIT_AMP_INT / pulse_df['Amplitude'].size
fa = 180
pulse_length_edit = args.pulse_dur_edit / 1E3  # In s.
pulse_amp_hz = 500 * (1E-3/pulse_length_edit) * (fa/180) * (1/amp_int_rel)
editing_pulse_hz = pulse_df['Amplitude'].to_numpy()\
    * np.exp(1j * pulse_df['Phase'].to_numpy())
editing_pulse_hz *= pulse_amp_hz


# Pulse offset
offset_frequency = -1.7 * 6.98 * 42.5774

# Use default factor
ref_pulse_hz = ref_pulse_cmplx * 500 * 2

# Gradients
# 20 mm isotropic voxel
# To do, bake this BW and voxel size dependent
ss_grad = [4.08, 0.0, 2.76, 2.76, 2.76, 0.0, 2.76]

# Symmetric excitation pulse rephase area
ss_rephaseArea = ss_grad[0] * pulse_length_exc * 0.5

# Calculate delays
# Delays excludes time with RF on.
# Output in s, calculation in us.
T_exc = pulse_length_exc * 1E6
T_mega = pulse_length_edit * 1E6
T_ref = pulse_length_ref * 1E6
T_echo = args.te * 1E3

raster_correction = 10 * np.ceil(0.1 * T_mega/2) - T_mega/2
padding = 3440 + 2 * raster_correction

tau1 = T_exc/2 + T_ref/2 + T_mega + padding
tau2 = T_ref + 3440
tau3 = tau2
tau4 = T_echo/2 - tau2
tau5 = tau4 - tau1

print(f'tau1 = {tau1}')
print(f'tau2 = {tau2}')
print(f'tau3 = {tau3}')
print(f'tau4 = {tau4}')
print(f'tau5 = {tau5}')

mega1_ref1 = T_ref/2 + T_mega/2 + padding/2
exc1_mega1 = tau1 - mega1_ref1

ref3_mega2 = mega1_ref1
mega2_ref4 = tau4 - ref3_mega2

delay1 = exc1_mega1 - (T_exc/2 + T_mega/2)  # exc to mega1
delay2 = mega1_ref1 - (T_ref/2 + T_mega/2)  # mega to ref1
delay3 = tau2 - T_ref                       # ref1 to ref2
delay4 = tau3 - T_ref                       # ref2 to ref3
delay5 = ref3_mega2 - (T_ref/2 + T_mega/2)  # ref3 to mega2
delay6 = mega2_ref4 - (T_ref/2 + T_mega/2)  # mega2 to ref4
delay7 = tau5 - T_ref/2                     # ref4 to ADC

delay_vec = np.array([delay1, delay2, delay3, delay4, delay5, delay6, delay7])
delay_vec /= 1E6
if args.water_suppression:
    # Insert a short delay
    delay_vec = np.insert(delay_vec, 0, 0.001)

if args.water_suppression:
    coherence_filter = [0, -1, None, 1, -1, 1, None, -1]
else:
    coherence_filter = [-1, None, 1, -1, 1, None, -1]

# Generate the json dict
json_dict = {}
json_dict["sequenceName"] = "eja_svs_mslaser",
record = {k: str(vars(args)[k]) for k in vars(args)}
arg_as_str = json.dumps(record)
abs_path = str(base_path.absolute())
json_dict["description"] = "7T MEGA-edited sLASER sequence, CMRR."\
                           f"Generated by generate.py in {abs_path}."\
                           f"Options: {arg_as_str}."
json_dict["B0"] = 6.98
json_dict["centralShift"] = 4.65

# Rx parameters
json_dict["Rx_Points"] = 8192
json_dict["Rx_SW"] = 6000
json_dict["Rx_LW"] = 1
json_dict["Rx_Phase"] = 0.0

# Simulation spatial extent
# Simulated for a 20 mm voxel. Double this
json_dict["x"] = [-20, 20]
json_dict["y"] = [-20, 20]
json_dict["z"] = [-20, 20]
json_dict["resolution"] = 3 * [args.resolution, ]

# Sequence description units
json_dict["RFUnits"] = "Hz"
json_dict["GradUnits"] = "mT"
json_dict["spaceUnits"] = "mm"

# Coherence filter
json_dict["CoherenceFilter"] = coherence_filter

# Delays
json_dict["delays"] = delay_vec.tolist()

# Rephase areas
if args.water_suppression:
    rp_areas = np.zeros((8, 3))
    rp_areas[1, 0] = -ss_rephaseArea
else:
    rp_areas = np.zeros((7, 3))
    rp_areas[0, 0] = -ss_rephaseArea
json_dict["rephaseAreas"] = rp_areas.tolist()


# Assemble the RF blocks
# Each block must contain:
# "time" duration of RF.
# "frequencyOffset" offset from the centralShift in Hz
# "phaseOffset" typically 0.
# "amp" pulse amplitude modulation
# "phase" pulse phase modulation
# "grad" [x,y,z] gradient
def gen_rf_array(edit_freq):
    rf_array = []
    # 0. Optional WS block
    if args.water_suppression:
        rf_array.append({
            "time": pulse_length_ws,
            "frequencyOffset": 0.0,
            "phaseOffset": 0.0,
            "amp": np.abs(ws_pulse_hz).tolist(),
            "phase": np.angle(ws_pulse_hz).tolist(),
            "grad": [0.0, 0.0, 0.0]})
    # 1. Excitation block
    rf_array.append({
        "time": pulse_length_exc,
        "frequencyOffset": offset_frequency,
        "phaseOffset": 0.0,
        "amp": np.abs(exc_pulse_hz).tolist(),
        "phase": np.angle(exc_pulse_hz).tolist(),
        "grad": [ss_grad[0], 0.0, 0.0]})
    # 2. MEGA block 1
    rf_array.append({
        "time": pulse_length_edit,
        "frequencyOffset": edit_freq,
        "phaseOffset": 0.0,
        "amp": np.abs(editing_pulse_hz).tolist(),
        "phase": np.angle(editing_pulse_hz).tolist(),
        "grad": [ss_grad[1], 0.0, 0.0]})
    # 3. Refocus block
    rf_array.append({
        "time": pulse_length_ref,
        "frequencyOffset": offset_frequency,
        "phaseOffset": 0.0,
        "amp": np.abs(ref_pulse_hz).tolist(),
        "phase": np.angle(ref_pulse_hz).tolist(),
        "grad": [0.0, ss_grad[2], 0.0]})
    # 4. Refocus block
    rf_array.append({
        "time": pulse_length_ref,
        "frequencyOffset": offset_frequency,
        "phaseOffset": 0.0,
        "amp": np.abs(ref_pulse_hz).tolist(),
        "phase": np.angle(ref_pulse_hz).tolist(),
        "grad": [0.0, ss_grad[3], 0.0]})
    # 5. Refocus block
    rf_array.append({
        "time": pulse_length_ref,
        "frequencyOffset": offset_frequency,
        "phaseOffset": 0.0,
        "amp": np.abs(ref_pulse_hz).tolist(),
        "phase": np.angle(ref_pulse_hz).tolist(),
        "grad": [0.0, 0.0, ss_grad[4]]})
    # 6. MEGA block 2
    rf_array.append({
        "time": pulse_length_edit,
        "frequencyOffset": edit_freq,
        "phaseOffset": 0.0,
        "amp": np.abs(editing_pulse_hz).tolist(),
        "phase": np.angle(editing_pulse_hz).tolist(),
        "grad": [ss_grad[5], 0.0, 0.0]})
    # 7. Refocus block
    rf_array.append({
        "time": pulse_length_ref,
        "frequencyOffset": offset_frequency,
        "phaseOffset": 0.0,
        "amp": np.abs(ref_pulse_hz).tolist(),
        "phase": np.angle(ref_pulse_hz).tolist(),
        "grad": [0.0, 0.0, ss_grad[6]]})
    return rf_array


# Generate OFF
edit_freq_off = (args.edit_ppm[0] - 4.65) * 6.98 * 42.5774
json_dict["RF"] = gen_rf_array(edit_freq_off)

# Write the json
if not args.output.exists():
    args.output.mkdir()
dat_str = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
file_name_base = f'eja_svs_mslaser_{dat_str}'
file_name_OFF = file_name_base + '_OFF.json'
with open(args.output / file_name_OFF, 'w') as fp:
    json.dump(json_dict, fp, indent=1)

# Generate ON
edit_freq_on = (args.edit_ppm[1] - 4.65) * 6.98 * 42.5774
json_dict["RF"] = gen_rf_array(edit_freq_on)
file_name_ON = file_name_base + '_ON.json'
# Write the json
with open(args.output / file_name_ON, 'w') as fp:
    json.dump(json_dict, fp, indent=1)

# generate script and default metab list
te_str = f'{args.te:0.0f}'
with open(args.output / 'run_sim.sh', 'w') as fp:
    fp.write('#!/bin/sh\n')
    fp.write(f'fsl_mrs_sim --overwrite -b metabs.txt -p -2 -o eja_svs_mslaser_{te_str}_off {file_name_OFF}\n')
    fp.write(f'fsl_mrs_sim --overwrite -b metabs.txt -p -2 -o eja_svs_mslaser_{te_str}_on {file_name_ON}\n')
    fp.write(f'basis_tools diff --add_or_sub sub eja_svs_mslaser_{te_str}_on eja_svs_mslaser_{te_str}_off eja_svs_mslaser_{te_str}_diff')

with open(args.output / 'metabs.txt', 'w') as fp:
    fp.write(
        '\n'.join(
            ['Ala', 'Asc', 'Asp', 'GPC', 'PCh', 'Cr', 'PCr', 'GABA', 'Glc', 'Gln', 'Glu', 'GSH', 'Ins', 'Lac', 'NAA', 'NAAG', 'PE', 'Scyllo', 'Tau']))

with open(args.output / 'run_test_sim.sh', 'w') as fp:
    fp.write('#!/bin/sh\n')
    fp.write(f'fsl_mrs_sim --overwrite -b test_metabs.txt -p -2 -o eja_svs_mslaser_{te_str}_off_test {file_name_OFF}\n')
    fp.write(f'fsl_mrs_sim --overwrite -b test_metabs.txt -p -2 -o eja_svs_mslaser_{te_str}_on_test {file_name_ON}\n')
    fp.write(f'basis_tools diff --add_or_sub sub eja_svs_mslaser_{te_str}_on_test eja_svs_mslaser_{te_str}_off_test eja_svs_mslaser_{te_str}_diff_test')

with open(args.output / 'test_metabs.txt', 'w') as fp:
    fp.write(
        '\n'.join(
            ['Cr', 'GABA', 'Lac']))
