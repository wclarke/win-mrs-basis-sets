uzay_svs_mpress
===============

MEGA-PRESS sequence. Apparently derived from the CMRR sequence.

This sequence can dual-band the editing pulse, to provide additional water suppression. It is important to model this properly if used (option on sequence special card 'MEGA water suppression'), as it also edits an NAA multiplet. As such there are two generation scripts: `generate.py` and `generate_db.py` (`db` for dual-band).

Excitation and Refocussing pulses
---------------------------------
I think that the pulses are:
* Excitation - hsinc_400_8750.hsinc_400_8750 (amp int 45.49)
* Refocus - mao_400_4.refoman6 (amp int 13.190)

These have typically been run with a pulse lengths of 2520 us and 7000 us for excitation and refocussing respectively.

Editing pulse lengths
---------------------
The min BW and thus pulse time (on the FMRIB scanner) achievable at:
* 68 ms TE is 53.00 Hz => 22.277 ms
* 80 ms TE is 41.52 Hz => 28.416 ms  

(This is with a excitation pulse duration of 2520 us and a refocus pulse duration of 7000 us.)

The amplitude integral is 277.145997509

Timings
-------
**WARNING** It appears that unless the editing pulse BW in minimised for a given TE the ADC might be inaccurately placed. It appears to be run ~1ms after the end of the editing pulse, not at a fixed time relative to the TE! There is a hint that this is fixed in the more recent CMRR versions of this sequence. __This might also be the default behaviour of some CMRR sequences, to open the ADC as early as possible and then remove additional points in processing/recon.__

For the above pulse durations and a 68ms TE the pulse centres occur at:  
Exc: 1240 + (2520/2) = 2500  
RF1: 4740 + 3500 = 8240  
Mega1: 15480 + (22272/2) = 26616   
RF2: 38740 + 3500 = 42240  
Mega2: 46720 + (22272/2) = 57856  
ADC = 70050  

For the above pulse durations and a 80ms TE the pulse centres occur at:  
Exc: 1240 + (2520/2) = 2500  
RF1: 4740 + 3500 = 8240  
Mega1: 15340 + (28416/2) = 29548  
RF2: 44740+ 3500 = 48240  
Mega2: 52720+ (28416/2) = 66928  
ADC = 82300  

Assume TE1 is fixed at 2*(8240 - 2500) = 11480  
For 68 ms TE2 is then 2*(42240 - (11480 + 2500)) = 56520  
For 80 ms TE2 is then 2*(48240 - (11480 + 2500)) = 68520  

**Delay calculations (working for 68 ms)**  
Delay 1 (Exc to RF1):  
TE1/2 - exc/2 - rf/2  
(11480/2) -  (2520/2)  -  (7000/2)  = 980  

Delay 2 (RF1 to MEGA1):  
((TE1/2 - rf/2) + (TE2/2 - rf/2) - mega)/2  
(((11480/2) -  (7000/2)) +  ((56520 /2) -  (7000/2)) -  22272)/2 = 2364  

Delay 3 (MEGA1 to RF2):  
((TE1/2 - rf/2) + (TE2/2 - rf/2) - mega)/2  
(((11480/2) -  (7000/2)) +  ((56520 /2) -  (7000/2)) -  22272)/2 = 2364  

Delay 4 (RF2 to MEGA2):  
(TE2/2 - rf/2) - mega)/2   
(((56520 /2) -  (7000/2)) -  22272)/2 = 1244  

Delay 5 (MEGA2 to ADC):  
(TE2/2 - rf/2) - mega)/2   
(((56520 /2) -  (7000/2)) -  22272)/2 = 1244  