mt_slaser (Bruker)
==================

Information about Mohamed Tachrount's sLASER implementation on the WIN 7T Bruker system (PV360).

Pulses
------
Excitation is SLR, and refocus is Sech.  
The pulse lengths were: 0.75 ms for the excitation and 3ms for the refocus  
They were applied: x y y z z  
Number of samples: 4048  
Bandwidth: 9.82 ppm (2949.85 Hz)  

Voxel and gradients
-------------------
Voxel size=1.5x1.5x1.1mm3  
The slice selection gradients were:  
VoxGard1 = 1.984467%  
VoxGard2 = 1.204855%  
VoxGard3 = 1.211943%  
Gmax=660 mT/m  

Timings
-------
The TEs were: TE1=6.5ms, TE2=5.5ms, TE3=5.5ms, and TE4=6.5ms  
Total TE = 24 ms

Delay 1:  
TE1/2 - Exc/2 - Ref/2  
(6.5/2) - (0.75/2) - (3/2) = 1.375 

Delay 2:  
TE1/2 + TE2/2 - Ref  
(6.5/2) + (5.5/2) - 3 = 3 

Delay 3:  
TE2/2 + TE3/2 - Ref  
(5.5/2) + (5.5/2) - 3 = 2.5 

Delay 4:  
TE3/2 + TE4/2 - Ref  
(5.5/2) + (6.5/2) - 3 = 3

 Delay 5:  
 TE4/2 - Ref/2  
 (6.5/2) - (3/2) = 1.75 

delays: 1.375 + 3 + 2.5 + 3 + 1.75 = 11.625  
pulses: 0.75/2 + 3*4 = 12.375  
Total: 12.375+11.625 = 24  