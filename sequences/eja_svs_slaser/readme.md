# EJA_SVS_SLASER - The CMRR Spectro package sLASER sequence

## Pulses
- 90 pulse is the hsinc_400_8750.hsinc_400_8750. Locally we have set a 3200 us duration at 3T
- 180s are HSn pulses controlled from the special card. Locally, at 3T, we have set these to 8000 us HS5 R=30, but simulation is recommended

See simulations in `bloch_sim_pulses.ipynb`

## Sequence timing calculations.

### Summary 
From exploring the timings I have guessed that the inter-pulse (centre to centre/ADC start, called tau 1-5) are calculated as such:
```
t2/3/4 = TE/4 and (t1 + t5 = TE/4)
t1 = 1580 + T_excitation/2 + T_refocus/2
t5 = TE/4 - t1
```
See `timing_calculations.xlsx`.

So the calculations relevant for the simulator are:
```
delay1 = tau1 - (T_exc/2 + T_ref/2)
delay2 = tau2 - T_ref
delay3 = tau3 - T_ref
delay4 = tau4 - T_ref
delay5 = tau5 - T_ref/2
```

### Timing measurements
The folder `sequence_timings` contains screenshots of the event blocks and sequence traces that help determine the timings.
I have made further notes below and calculations in `timing_calculations.xlsx`

__NOTE__ These timings come with setting gradient ramp time to 140 us (default for Prisma?).

#### Timings for the 60 ms TE (3.2 ms excite 8ms refocus)

```Excite at 1140, for 3200, centre at 1140 + 3200/2 = 2740
RF1 at 5920, for 8000, centre at 5920 + 4000 = 9920 
RF2 at 20920, for 8000, centre at 20920 + 4000 = 24920 
RF3 at 35920, for 8000, centre at 35920 + 4000 = 39920
RF4 at 50920, for 8000, centre at 50920 + 4000 = 54920 
ADC at 60540, logs say 62740
```
```
delay1 = 5920 - 4340 = 1580
delay2 = 20920 - (5920 + 8000) = 7000
delay3 = 35920 - (20920 + 8000) = 7000
delay4 = 50920 - (35920 + 8000) = 7000
delay5 = 62740 - (50920 + 8000) = 3820 
```

taus are calculated centre-to-centre 
```
tau1 = 9920 - 2740 = 7180
tau2 = 15000
tau3 = 15000
tau4 = 15000
tau5 = 7820
```

####  Timings for the 80 ms TE
```
Excite at 1140, for 3200, centre at 1140 + 3200/2 = 2740
RF1 at 5920, for 8000, centre at 5920 + 4000 = 9920 
RF2 at 25920, for 8000, centre at 25920 + 4000 = 29920 
RF3 at 45920, for 8000, centre at 45920 + 4000 = 49920
RF4 at 65920, for 8000, centre at 65920 + 4000 = 69920 
ADC at 75540, logs say 82740
```

```
delay1 = 5920 - 4340 = 1580
delay2 = 25920 - (5920 + 8000) = 12000
delay3 = 45920 - (25920 + 8000) = 12000
delay4 = 65920 - (45920 + 8000) = 12000
delay5 = 82740 - (65920 + 8000) = 8820
```

```
tau1 = 7180
tau2 = 20000
tau3 = 20000
tau4 = 20000
tau5 = 12820
```

#### Timings for the 100 ms TE
TO DO: They follow above trend

#### Timings for the 60 ms TE - 2ms excite
```
Excite at 1140, for 2000, centre at 1140 + 2000/2 = 2140
RF1 at 4720, for 8000, centre at 4720 + 4000 = 8720 
RF2 at 19720, for 8000, centre at 19720 + 4000 = 23720 
RF3 at 34720, for 8000, centre at 34720 + 4000 = 38720
RF4 at 49720, for 8000, centre at 49720 + 4000 = 53720 
ADC at 59440, logs say 62140
```
```
delay1 = 4720 - 3140 = 1580
delay2 = 19720 - (4720 + 8000) = 7000
delay3 = 34720 - (19720 + 8000) = 7000
delay4 = 49720 - (34720 + 8000) = 7000
delay5 = 62140 - (49720 + 8000) = 4420
```
```
tau1 = 6580
tau2 = 15000
tau3 = 15000
tau4 = 15000
tau5 = 8420
```
#### Timings for the 60 ms TE - 2ms excite 4ms ref
```
Excite at 1140, for 2000, centre at 1140 + 2000/2 = 2140
RF1 at 4720, for 4000, centre at 4720 + 2000 = 6720 
RF2 at 19720, for 4000, centre at 19720 + 2000 = 21720 
RF3 at 34720, for 4000, centre at 34720 + 2000 = 36720
RF4 at 49720, for 4000, centre at 49720 + 2000 = 51720 
ADC at 55440, logs say 62140
```
```
delay1 = 4720 - 3140 = 1580
delay2 = 19720 - (4720 + 4000) = 11000
delay3 = 34720 - (19720 + 4000) = 11000
delay4 = 49720 - (34720 + 4000) = 11000
delay5 = 62140 - (49720 + 4000) = 8420
```
```
tau1 = 4580
tau2 = 15000
tau3 = 15000
tau4 = 15000
tau5 = 10420
```

####  Timings for the 35 ms TE - 1.8ms excite 4ms ref
```
Excite at 1140, for 1800, centre at 1140 + 1800/2 = 2040
RF1 at 4520, for 4000, centre at 4520 + 2000 = 6520 
RF2 at 13270, for 4000, centre at 13270 + 2000 = 15270 
RF3 at 22020, for 4000, centre at 22020 + 2000 = 24020
RF4 at 30770, for 4000, centre at 30770 + 2000 = 32770 
ADC at 36840, logs say 37040
```
```
tau1 = 6520 - 2040 = 4480
tau2 = 8750
tau3 = 8750
tau4 = 8750
tau5 = 4270
```
```
delay1 = 4520 - (1140 + 1800) = 1580
delay2 = 13270 - (4520 + 4000) = 4750
delay3 = 22020 - (13270 + 4000) = 4750
delay4 = 30770 - (22020 + 4000) = 4750
delay5 = 37040 - (30770 + 4000) = 2270
```