# EJA_SVS_PRESS

The [CMRR Spectro Package](https://www.cmrr.umn.edu/spectro/) PRESS sequence.

Simulated here for a 3T Prisma scanner.

## Pulses
Pulses listed in binary:
- hsinc_400_8750
- mao_400_4
- RR_6ms_1k4_22uT
- RR_6ms_2k0_25uT

### Excitation
#### hsinc_400_8750  
Amp int = 45.490761

### Refocus
Either `mao_400_4` or if the "RR refoc. pulse" option is selected it will use 
the RR pulses (which? They are very different in profile)

From instructions "Two new pulses, root reflected, with higher bandwidth are available for refocusing. First RR pulse uses very similar power to the default mao pulses, but provides 40% higher bandwidth. Second RR pulse requires more power than mao pulse, and provides two times higher bandwidth." 

Note to select the highest bandwidth you first select "RR refoc. pulse" (which requires a long pulse time to be set), then a new box "High BW 'RR' pulse" appears.

#### mao_400_4
Amp int = 36.711385

#### RR_6ms_1k4_22uT
Comment: "1.4kHz bw at 6 ms, 22 ut"  
Amp int = 22.785320

#### RR_6ms_2k0_25uT
Comment: "2kHz bw at 6 ms, 25 ut"  
Amp int = 20.051080

## Timing
First three timings listed in each case below are pulse centres

### Case 1
For 35 ms TE with automatic TE1 minimisation
2600 ms excitation
5200 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 2440
180 pulse #1       = 7920
180 pulse #2       = 25420
acq start          = 37440
actual tau1        = 5480
actual tau2        = 12020
expected tau1+tau2 = 17500
  actual tau1+tau2 = 17500
expected te        = 35000
  actual te        = 35000
****END TIMING***************************
```

### Case 2
For 45 ms TE with automatic TE1 minimisation
2600 ms excitation
5200 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 2440
180 pulse #1       = 7920
180 pulse #2       = 30420
acq start          = 47440
actual tau1        = 5480
actual tau2        = 17020
expected tau1+tau2 = 22500
  actual tau1+tau2 = 22500
expected te        = 45000
  actual te        = 45000
****END TIMING***************************
```

### Case 3
For 135 ms TE with automatic TE1 minimisation
2600 ms excitation
5200 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 2440
180 pulse #1       = 7920
180 pulse #2       = 75420
acq start          = 137440
actual tau1        = 5480
actual tau2        = 62020
expected tau1+tau2 = 67500
  actual tau1+tau2 = 67500
expected te        = 135000
  actual te        = 135000
****END TIMING***************************
```

### Case 4
For 35 ms TE with automatic TE1 minimisation
1000 ms excitation
5200 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 1640
180 pulse #1       = 6320
180 pulse #2       = 23820
acq start          = 36640
actual tau1        = 4680
actual tau2        = 12820
expected tau1+tau2 = 17500
  actual tau1+tau2 = 17500
expected te        = 35000
  actual te        = 35000
****END TIMING***************************
```

### Case 5
For 35 ms TE with automatic TE1 minimisation
2600 ms excitation
3000 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 2440
180 pulse #1       = 6820
180 pulse #2       = 24320
acq start          = 37440
actual tau1        = 4380
actual tau2        = 13120
expected tau1+tau2 = 17500
  actual tau1+tau2 = 17500
expected te        = 35000
  actual te        = 35000
****END TIMING***************************
```


### Case 6
For 35 ms TE with  TE1 set to 15
2600 ms excitation
3000 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 2440
180 pulse #1       = 9940
180 pulse #2       = 27440
acq start          = 37440
actual tau1        = 7500
actual tau2        = 10000
expected tau1+tau2 = 17500
  actual tau1+tau2 = 17500
expected te        = 35000
  actual te        = 35000
****END TIMING***************************
```

### Case 7
For 35 ms TE with automatic TE1 minimisation
1000 ms excitation
1000 ms mao refocus

```
****TIMING: PRESS************************
90 pulse #1        = 1640
180 pulse #1       = 4220
180 pulse #2       = 21720
acq start          = 36640
actual tau1        = 2580
actual tau2        = 14920
expected tau1+tau2 = 17500
  actual tau1+tau2 = 17500
expected te        = 35000
  actual te        = 35000
****END TIMING***************************
```

1300 + 1500 + 1580 = 4380
So the base time for tau1 seems to be 1580 + 0.5 *refocus + 0.5 * excite, but this will depend on slew rates etc.
