slaser_concept_dw_real32
========================

CONCEPT 7T sLASER sequence. 16 ring version

Excitation and Refocussing pulses
---------------------------------
From the binary file %CustomerSeq%/extrf_ue_2012_01_24.dat  

* Excitation - asym_90exc.asym_90exc   (amp int 61.075)
* Refocus - HS4_R25.HS4_R25   (amp int 56.67)

These have typically been run with a pulse lengths of 6000 us and 5000 us for excitation and refocussing respectively.
