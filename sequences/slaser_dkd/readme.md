# slaser_dkd (CMRR Spectro package)

These simulations produce basis spectra for Dinesh Deelchand's sLASER sequence. This is labelled as `SEMI-LASER (MRM 2011, NMB 2019) Release 2016-12` at https://www.cmrr.umn.edu/spectro/.

Theoretically these simulations should also be valid for the 'universal' sLASER sequence described in the [NMB 2019 publication](http://www.ncbi.nlm.nih.gov/pubmed/31854045).

## Pulses
Many of the pulses are generated at run time. Adam Berrington kindly provided the original matlab functions that described the pulses, which have been translated into `goia_pulses.py`.