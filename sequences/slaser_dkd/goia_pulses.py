"""Functions to generate the FOCI, GOIA-HS and GOIA-WURST pulses
in the slaser_dkd (universal sLASER) sequence.

Originally based on code from Adam Berrington and Dinesh Deelchand,
and based on Andronesi et al. JMR 2010 paper.

This python port was done by Will Clarke, University of Oxford, 2023
"""

import numpy as np
import scipy.integrate as it


def gen_goia_pulse(tp_ms: float, R: float, pulsetype: str, n: int, m: int, f: float, plot=False):
    """Generate the modulation functions for the refocussing
    pulses in the universal sLASER sequence.

    :param tp_ms: Pulse duration in ms
    :type tp_ms: float
    :param R: Time-bandwidth product
    :type R: float
    :param pulsetype: Pulse type selection: FOCI, HS, WURST
    :type pulsetype: string
    :param n: order of B1 amplitude modulation, i.e. HSn
    :type n: int
    :param m: order of gradient modulation, i.e. HSn
    :type m: int
    :param f: Something to do with the gradients
    :type f: float
    :param plot: Plot modulation functions, defaults to False
    :type plot: bool, optional
    :return: Dictionary of pulse modulations
    :rtype: dict
    """

    Tp = tp_ms * 1e-3  # in s
    numstep = int(Tp * 1e6 / 10)  # 10us dwell time on Siemens

    # Timing
    beta = np.arccosh(1 / 0.01)  # Equaivalent to arcsech, 10% cut off
    t = np.linspace(0, Tp, numstep)
    tau = (2 * t / Tp) - 1

    # Modulation parameters and Equations
    # Amplitude
    B1max = 1

    def sech(x):
        return 1 / np.cosh(x)

    if pulsetype in ['HS', 'FOCI']:

        if pulsetype == 'FOCI':
            # shaping function, i.e. C-shaped, Kinchesh et al. JMR 2005
            fF = 10  # 100-f*100
            temp2 = 1 / sech(beta * tau)
            AA = []
            for tau_step, tmp2 in zip(tau, temp2):
                if tmp2 > (1 / fF):
                    AA.append(np.cosh(beta * tau_step))
                else:
                    AA.append(fF)
            AA = np.asarray(AA)
            # for gradient
            AAG = AA / fF

            # reset params
            f = 0  # need to set OFF gradient modulation factor
            n = 1  # order of B1 modulation, e.g. HSn

        else:
            AA = 1
            AAG = 1

        # Amplitude modulation
        B1 = AA * B1max * sech(beta * tau ** n)

        # Gradient waveform
        G = AAG * (1 - f * sech(beta * tau ** m))

        # Frequency modulation
        def freq(x):
            return (B1max * sech(beta * x ** n)) ** 2 / ((1 - f * sech(beta * x ** m)))

    elif pulsetype == 'WURST':
        # Amplitude modulation
        B1 = B1max * (1 - np.abs(np.sin(np.pi / 2 * tau)) ** n)

        # Gradient waveform
        G = (1 - f) + f * np.abs(np.sin(np.pi / 2 * tau)) ** m

        # Frequency modulation
        def freq(x):
            return (B1max * (1 - np.abs(np.sin(np.pi / 2 * x)) ** n)) ** 2\
                 / (((1 - f) + f * np.abs(np.sin(np.pi / 2 * x)) ** m))

    else:
        raise ValueError('Type of RF pulse NOT defined. Must be one of: FOCI, HS, WURST')

    # Freq modulation
    # cumulative integration 
    intval3 = it.cumulative_trapezoid(freq(tau), tau, initial=0)
    # wc is center freq where w(Tp/2)=0, i.e. freq at middle of pulse is 0
    intval3 -= intval3[int(intval3.size / 2)]

    # freq modulation
    w1 = G * intval3
    A = R / (2*Tp)
    w = (w1 / np.max(w1)) * A

    # Phase modulation
    # using cumulative trapz integration
    Pfinal = 2 * np.pi * it.cumulative_trapezoid(w, t, initial=0)

    # shift phase to have zero value at bottom
    Pshift = Pfinal - np.min(Pfinal)
    Pdeg = np.rad2deg(Pshift)

    # display result
    if plot:
        import matplotlib.pyplot as plt
        fig, axes = plt.subplots(2, 2)
        t_axis = t * 1E3
        axes[0][0].plot(t_axis, B1)
        axes[0][0].set_title('Amplitude mod')
        axes[0][0].set_ylabel('kHz')
        axes[0][0].set_xlabel('T_p (ms)')

        axes[0][1].plot(t_axis, w/1e3) 
        axes[0][1].set_title('Frequency mod')
        axes[0][1].set_ylabel('kHz')
        axes[0][1].set_xlabel('T_p (ms)')

        axes[1][0].plot(t_axis, G) 
        axes[1][0].set_title('Gradient shape')
        axes[1][0].set_ylabel('G/Gmax')
        axes[1][0].set_xlabel('T_p (ms)')

        axes[1][1].plot(t_axis, (Pdeg)) 
        axes[1][1].set_title('Phase mod')
        axes[1][1].set_ylabel('deg')
        axes[1][1].set_xlabel('T_p (ms)')
        plt.show()

    return dict(
        amp=B1,
        phz=Pdeg,
        grad=G,
        tp_ms=Tp*1e3,
        tbwp=R)
