function [out, mz,  dp] = simulB1Goia2_ab(pulseType,Tpms,HSn,RkHz,Gradm,fMod,B1vals,myOffset,isplot)
%*************************************************
% function out= simulB1Goia2(pulseType,Tpms,HSn,RkHz,Gradm,fMod,B1vals,myOffset)
% FOCI, GOIA-HS or GOIA-WURST gradient-modulated RF pulses
% - RF and gradient waveforms are generated internally
% Able to array B1, Rval, Offsets
%
% Dinesh Deelchand, CMRR, 14 September 2015
% Updated 29 Sept 2015
%*************************************************

% initial values
if (nargin == 0) || (nargin < 7)
    disp('Use simulB1Goia2(pulseType,Tpms,HSn,RkHz,Gradm,fMod,B1vals,myOffset)');
    return
end
if(nargin<9) % APB
    isplot = 0;
end

if (nargin<8)
    myOffset = 0;
end

% offset can only be arrayed on it's own
if (((size(RkHz,2)>1) || (size(B1vals,2)>1)) && (size(myOffset,2)>1))
    fprintf('Multiple offsets work only if nothing else is arrayed!\n');
    out=[];
    return
end

%initialize
Npts = 1000;
thk=2; % thickness in cm
dp = linspace(-3,3,Npts);
MXY = zeros(length(dp),length(B1vals),size(RkHz,2));
MZ = MXY;

for Rvalidx = 1:size(RkHz,2)
    % simulate GOIA/FOCI RF and grad waveforms
    if (strcmp(pulseType,'FOCI') &&  (HSn>1) )
        HSn = 1;
        disp('*** Reseting HS1 pulse type ***')
    end
    myData = genGoiaFoci2(Tpms,RkHz(Rvalidx),pulseType,HSn,Gradm,fMod);
    rf = myData.RF;
    grad = myData.Grad;
    Tp = Tpms*1e-3;
    
    
    %% RF waveform
    %normalized
    rfUnity = rf.amp./max(rf.amp);
    
    %phase
    phz = deg2rad(rf.phz);
    re=rfUnity.*cos(phz);
    im=rfUnity.*sin(phz);
    rfs=(re+1i*im)';
    
    % normalize
    gamma=4257;  %Hz/G
    mypulse = rfs./sum(real(rfs));  % normalize sum(rf)=1
    mypulseG = mypulse/(2*pi*4.257e3*Tp/length(mypulse));
    mypulseHz = mypulseG*gamma;   % convert to Hz
    mypulseHzNorm = mypulseHz/max(abs(mypulseHz));
    % rescaled based on user-defined B1/Hz
    
    
    %% gradient waveform
    np=length(mypulseHzNorm);
    dt=Tp/np;
    
    g = (RkHz(Rvalidx)/Tp)/(gamma*thk);
    gx = grad'.*g;
    
    
    %% loop thru B1 values
    Bvalidx = 1;
    df = myOffset;
    
    for B1max = B1vals
        fprintf('                            B1max = %5.2f Hz \n', B1max);
        % RF pulse at B1max value
        mypulseHzreq = mypulseHzNorm*B1max;
        
        %convert B1 to Gauss
        mypulseGnew = mypulseHzreq/gamma;
        
        % Bloch simulator
        MX0 = 0.*ones(length(dp),length(df));
        MY0 = 0.*ones(length(dp),length(df));
        MZ0 = 1.*ones(length(dp),length(df));
        [mx,my,mz] = bloch(mypulseGnew,gx,dt,100,100,df,dp,0,MX0,MY0,MZ0);
        
        mxy = mx+1i*my;
        
        % put in my own Bloch Sim here
%         if(1)
%             sys.gamma = gamma * 1e4;
%             RF.N = length(mypulseGnew);
%             RF.B1 = mypulseHzreq/(sys.gamma);
%             sim.M0 = [0,0,1]';
%             RF.G = 0;
%             RF.tstep = dt;
%             [M, M_array] = pulse_M_profiles(10e4, 10e3, sys, RF, sim)
%         end
%         
        if (size(df,2)==1)
            % keep MXY and MZ values in memory
            MXY(:,Bvalidx,Rvalidx) = mxy;
            MZ(:,Bvalidx,Rvalidx) = mz;
        else
            MXY=mxy;
            MZ=mz;
            
        end
        
        
        Bvalidx = Bvalidx + 1;
    end
    
end


if (Bvalidx-1)==1
    
    if (size(MZ,2)==1 && size(MZ,3)==1)
        if(isplot) %APB
        disp('a1')
        figure, plot(dp,abs(mxy)), grid on; hold on, plot(dp,real(mz),'r');
        legend('Mxy','Mz'); xlabel('Position (cm)');
        end
    else
        disp('1b')
        figure, grid on; hold on, plot(dp,real(squeeze(MZ)));
        xlabel('Position (cm)');
        if (size(RkHz,2)>1) myLegend = cellstr(num2str(RkHz')); legend(myLegend,'Location','Best'), end
        if (size(df,2)>1) myLegend = cellstr(num2str(df')); legend(myLegend,'Location','Best'), end
        
    end
    
    if nargout
        out = [mxy mz];
    end
else
    disp('2')
    figure,
    mystr = sprintf('Middle of slice (%2.1f ms at R=%3.1f kHz)',Tpms,RkHz(Rvalidx)');
    MZplot = squeeze(MZ(end/2,:,:));
    plot(B1vals,MZplot), title(mystr), grid on
    axis([B1vals(1) B1vals(end) -1 1]), xlabel('B1 /Hz'), ylabel('Mz')
    
    if (size(MZ,3)==1)
        figure, hold on,
        plot(dp,MZ)
        grid on, xlabel('Position /cm'), ylabel('Mz')
        myLegend = cellstr(num2str(B1vals')); legend(myLegend,'Location','EastOutside')
    end
end


% display pulse info
%fprintf('\n %s pulse duration of %2.1f ms\n',pulseType,Tpms);


return






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                 %
% Generate GOIA pulse + gradient waveform                         %
% based on Andronesi et al. JMR 2010 paper                        %
%                                                                 %
% Dinesh Deelchand,                                               %
% CMRR, University of Minnesota, 23 April 2010                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function myOut =genGoiaFoci2(Tpms,R,pulsetype,n,m,f)

Tp = Tpms*1e-3;  % in s
numstep = Tp*1e6/10;  % 10us dwell time on Siemens


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% timing
beta = asech(0.01);		%10% cut off
t = linspace(0,Tp,numstep);
tau = 2*t./Tp - 1;

%***************************************************
% Modulation parameters and Equations
%***************************************************
% Amplitude
B1max = 1;

if (strcmp(pulsetype,'HS') ||  strcmp(pulsetype,'FOCI'))
    
    if strcmp(pulsetype,'FOCI')
        %shaping function, i.e. C-shaped, Kinchesh et al. JMR 2005
        fF = 10;  %100-f*100
        temp2 = sech(beta.*tau);
        for ictr =1:length(temp2)
            if (temp2(ictr) > 1/fF)
                AA(ictr) = cosh(beta.*tau(ictr));
            else
                AA(ictr) = fF;
            end
        end
        % for gradient
        AAG = AA./fF;
        
        % reset params
        f = 0; % need to set OFF gradient modulation factor
        n = 1;  %order of B1 modulation, e.g. HSn
        
    else
        AA = 1;
        AAG = 1;
    end
    
    % Amplitude modulation
    B1 = AA.*B1max.*sech(beta*tau.^n);
    
    % Gradient waveform
    G = AAG.*(1 - f*sech(beta*tau.^m));
    
    % Frequency modulation
    feq =@(x) (B1max.*sech(beta*x.^n)).^2 ./((1 - f*sech(beta*x.^m)));
    
    
elseif strcmp(pulsetype,'W')
    % Amplitude modulation
    B1 = B1max.*(1 - abs(sin(pi/2*tau)).^n);
    
    % Gradient waveform
    G = ((1 - f) + f*abs(sin(pi/2*tau)).^m);
    
    % Frequency modulation
    feq =@(x) (B1max.*(1 - abs(sin(pi/2*x)).^n)).^2./(((1 - f) + f*abs(sin(pi/2*x)).^m));
    
else
    disp('Type of RF pulse to generate NOT defined');
    return
end

%***************************************************
%% Freq modulation
%***************************************************
% cumulative integration built-in Matlab script
intval3 = cumtrapz(tau,feq(tau));
% wc is center freq where w(Tp/2)=0, i.e. freq at middle of pulse is 0
wc = intval3(end/2);
intval3 = intval3 - wc;

% freq modulation
w1 = G.*intval3;
A = R/(2*Tp);
w = w1./max(w1) * A;


%***************************************************
%% Phase modulation
%***************************************************
% using cumulative trapz integration
Pfinal = 2*pi*cumtrapz(t,w);

% shift phase to have zero value at bottom
Pshift = Pfinal-min(Pfinal);
Pdeg = rad2deg(Pshift);


%***************************************************
% display result
%***************************************************
if(0) %APB disabled
figure,
subplot(221), plot(t*1e3,B1); title('Amplitude mod'), ylabel('kHz'), xlabel('ms'); grid on
subplot(222), plot(t*1e3,w/1e3); title('Frequency mod'), ylabel('kHz'), xlabel('ms');grid on
subplot(223), plot(t*1e3,G); title('Gradient shape'), ylabel('G/Gmax'), xlabel('ms'); grid on
subplot(224), plot(t*1e3,(Pdeg)); title('Phase mod'), ylabel('deg'), xlabel('ms');grid on
end

% output
if (nargout>0)
    myOut.RF.amp = B1;
    myOut.RF.phz = Pdeg;
    myOut.RF.pwms = Tp*1e3;
    myOut.RF.bw = R;
    myOut.Grad = G;
end

return

