function [ deg_ang ] = rad2deg( rad_ang )

    deg_ang = rad_ang * 180 / pi;

end

