function [ rad_ang ] = deg2rand( deg_ang )

    rad_ang = deg_ang * pi / 180;

end

