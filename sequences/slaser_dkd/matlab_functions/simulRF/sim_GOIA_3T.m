BW = 10; %kHz
dur = 4.5;
n = 16;
m = 4;
grad_factor = 0.90;

B1max = 15*42.577; % 15uT in Hz 
Npts = 1000;
dp = linspace(-3,3,Npts);

%settings for 3 T
outOn=simulB1Goia2_ab('W',dur,n,dur*BW,m,grad_factor,B1max,0);
figure
plot(dp,real(outOn(:,2)))
