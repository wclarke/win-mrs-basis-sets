% edit for 7T
% siya sherif
% 30MAR2022
%--------------------------------------------------------------------------
BW  = 45    ;% 10; %kHz     % Sequence:Special - Bandwidth_1ms - 45 kHz
dur = 5.0   ;% 4.5;         % Sequence:Special - Exitataion duration - 1500 us + Refocus duration - 3500 us 
n   = 16;   ;               % Sequence:Special - HSn modulation -16
m   = 4;
grad_factor = 0.85; % 0.90; % Sequence:gradient factor - 85 %

B1max = 15*42.577; % 15uT in Hz 
Npts = 1000;
dp = linspace(-3,3,Npts);

%settings for 3 T
outOn=simulB1Goia2_ab('W',dur,n,dur*BW,m,grad_factor,B1max,0);
figure
plot(dp,real(outOn(:,2)))
